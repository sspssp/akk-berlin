<?php


/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/

session_start();
//check if user is logged in
if(!isset($_SESSION['uid_akk']))
{
	Header("Location: index.php");
	exit(); 
}

include ('header.php');

require_once( 'database.php' );

$db=new DataBase;

$res=$db->GetDivisions();

/*
shows all which has paid live, by deivsion
*/
while( $row=$db->mysql_fetch_row($res ) ){

	$res2=$db->GetBackupListByDivision( $row[0] );


	print '<h1>'.$row[0].'</h1>';


	print '<table>';

	print '<thead><tr><b><td>Nachname</td><td>Zwischenname</td><td>Vorname</td>
		<td>Mitgliedsnummer</td><td>Geburtsdatum</td><td>Adresse</td>
		<td>Ort</td><td>bezahlt</td><td>Offen</td></b></tr></thead>';

	while( $row2=$db->mysql_fetch_row( $res2 ) ){

		print '<tr>';

		if( $row2[8]=='1' ){
			for( $i=0;$i<7;$i=$i+1 ){
				print '<td><strike>'.$row2[$i].'</strike></td>';
			};
		} else {
			for( $i=0;$i<7;$i=$i+1 ){
				print '<td>'.$row2[$i].'</td>';
			};
		};

		if( $row2[8]=='1' ){
			print '<td><strike>';
		} else {
			print '<td>';
		};

		if( $row2[7]=='1' ){
			print '<span style="color:#007f3f;">JA</span>';
		} else {
			print '<span style="color:#bf0000;">NEIN</span></td><td>'.$row2[9].'€</td>';
		};


		if( $row2[8]=='1' ){
			print '</strike></td>';
		} else {
			print '</td>';
		};

		

		print '</tr>';

	};

	print '</table>';

	print '<br>';

};


include ('footer.php');

