<?php
/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de>
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As
* long as you retain this notice you can do whatever you want with this stuff.
* If we meet some day, and you think this stuff is worth it, you can buy us a
* beer in return
*
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/

session_start();
//check if user is logged in
if(!isset($_SESSION['uid_akk']))
{
        Header("Location: index.php");
        exit();
}

require_once( 'AkkFunctions.php' );

/*
prints a hidden input field of name "active" and value "on" if something has happend in the last search

this includes akk oder deakk operation.

Furthermore it prints the field if your start over the application
*/
function PrintActive() {

        if( isset( $_POST['deakk'] ) or isset( $_POST['akk'] ) ){
                print '<input type="hidden" name="active" value="on">';
        };


        if( !isset( $_GET['superfield']) and !isset( $_GET['lastname']) and
         !isset( $_GET['exactlastname']) and !isset( $_GET['firstname'])  and
         !isset( $_GET['exactfirstname']) and !isset( $_GET['membershipid']) and
         !isset( $_GET['exactmembershipid']) ){
                print '<input type="hidden" name="active" value="on">';
        };
}

include ('header.php');

$db=new DataBase;
?>
<form method="GET" action="Wahlkreis.php">
<input name="street" value="<?php echo urldecode($_GET["street"]); ?>"><input name="hausnummer" value="<?php echo $_GET["hausnummer"]; ?>"><br>
<input name="limit" value="5">*²<br>
<input type="submit" value="Suchen">
</form>
<?php
$limit=5;
if(isset($_GET["limit"]))
{
$limit = $_GET["limit"];
}
$w = $db->getWahlkreis(urldecode($_GET["street"]), $_GET["hausnummer"], $limit);
echo "<table>";
echo "<tr><th>Strase</th><th>Hausnummer Von</th><th>Hausnummer bos</th><th>HausnummernZusatz von</th><th>HausnummernZusatz bis</th><th>Typ*</th><th>PLZ</th><th>Wahlkreis</th></tr>";
foreach($w as $wahlkreis)
{
	echo "<tr><td>".$wahlkreis["Strassenname"]."</td><td>".$wahlkreis["HausnrVon"]."</td><td>".$wahlkreis["HausnrBis"]."</td><td>".$wahlkreis["NausnrzusatzVon"]."</td><td>".$wahlkreis["HausnrzusatzBis"]."</td><td>".$wahlkreis["Nummerierungskennzeichen"]."</td><td>".$wahlkreis["PLZ"]."</td><td>".$wahlkreis["Bundestagswahlkreis"]."</td></tr>";
}
echo "</table>";
?>
* G = Grade Hausnummern<br>
U = Ungrade Hausnummern<br>
F = Fortlaufend<br>
*² Es werden nur die ersten Zeichen des Straßennames verglichen, mit dieser Einstellung kann die Anzahl der Zeichen geandert werden<br>
