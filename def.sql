
/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/




CREATE USER 'user'@'localhost' IDENTIFIED BY 'userpasswd';
GRANT ALL ON members.* TO 'user'@'localhost';

CREATE database members;

use members;


/*
	members table
*/
CREATE TABLE members (
	dbid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	lastname CHAR(100) NOT NULL,
	middlename CHAR(100),
	firstname CHAR(100) NOT NULL,
	membershipid INT NOT NULL UNIQUE,
	birthdate DATE NOT NULL,
	adress CHAR(100),
	city CHAR(100),
	zipcode CHAR(5),
	division CHAR(100) NOT NULL,
	paid TINYINT(1) DEFAULT 1,
	akkreditiert TINYINT(1) DEFAULT 0,
	paidchanged TINYINT DEFAULT 0,
	acomment CHAR(100) DEFAULT "",
	warning INT NOT NULL DEFAULT 0,
	topay INT
) 
 CHARACTER SET utf8 COLLATE utf8_general_ci
;


LOAD DATA LOCAL INFILE '/opt/lampp/htdocs/Akk/db.csv' INTO TABLE members  
	CHARACTER SET utf8
	FIELDS TERMINATED BY ','
	LINES TERMINATED BY '\n'  
	(lastname,middlename,firstname,membershipid,birthdate,adress,city,zipcode,division,paid,akkreditiert,warning);



UPDATE members SET akkreditiert=0 WHERE paid=0;
UPDATE members SET topay=36 WHERE paid=0 and akkreditiert=0;

/*
	and the user table
*/
CREATE TABLE users (
	userid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name CHAR(100) NOT NULL UNIQUE,
	hash CHAR(100) NOT NULL
);

INSERT INTO users (name,hash) VALUES ('akk1' , md5('akk1') );
INSERT INTO users (name,hash) VALUES ('akk2' , md5('akk2') );
INSERT INTO users (name,hash) VALUES ('akk3' , md5('akk3') );
INSERT INTO users (name,hash) VALUES ('akk4' , md5('akk4') );

/*
	change table


	thechange =>
		0 => akk
		1 => deakk
		2 => pay
		3 => unpay
*/
CREATE TABLE changes (
	changeid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	userid INT NOT NULL,
	dbid INT NOT NULL,
	thechange INT NOT NULL,
	acomment CHAR(100),
	thetime DATETIME NOT NULL
);


