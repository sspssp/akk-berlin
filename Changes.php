<?php
/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/


session_start();
//check if user is logged in
if(!isset($_SESSION['uid_akk']))
{
	Header("Location: index.php");
	exit(); 
}

require_once( 'AkkFunctions.php' );

include ('header.php');

$db=new DataBase;

?>

	<form type="get" action="<?php print GetCurrentURL(); ?>">

		<label>DB ID:</label><input type="text" name="dbid" value="<?php print GetGet('dbid'); ?>">
		<input type="submit" value="Suchen">
	</form>

	<br><br>

<?php

function PrintUser( $row )
{

	print '<table>';

	print '<thead><tr><td>Nachname</td><td>Zwischenname</td><td>Vorname</td><td>Mitgliedsnummer</td>
	 <td>Geburtsdatum</td><td>Adresse</td><td>Ort</td><td>PLZ</td><td>LV</td></tr></thead>';

	print '<tr>';
	for( $i=0;$i<9;$i++ ){ print '<td>'.$row[$i].'</td>'; };
	print '</tr>';

	print '</table>';

}

function PrintChangeTable( $db , $res )
{

	print '<table>';

	print '<thead<tr><td>Userid</td><td>User</td><td>DB-ID</td><td>Änderung</td><td>Kommentar</td><td>Zeit</tr></thead';

	while( $row=$db->mysql_fetch_row( $res ) ){

		print '<tr>';



		print '<td>'.$row[1].'</td>';

		print '<td>';
		$row2=$db->GetUserById( $row[1] );
		print $row2[0];
		print '</td>';

		print '<td>'.$row[2].'</td>';


		print '<td>';
		switch ($row[3]) {
			case "0": print 'Akk';break;
			case "1": print 'DeAkk';break;
			case "2": print 'Pay';break;
			case "3": print 'Unpay';break;
		};
		print '</td>';



		print '<td>';
		if( $row[4]!="NULL" ){
			print $row[4];
		};
		print '</td>';

		print '<td>'.$row[5].'</td>';

		print '</tr>';

	};

	print '</table>';

};

// do the work

if( isset( $_GET['dbid'] ) ){

	if( $_GET['dbid']!="" ){

		$row=$db->GetMemberByID ( $_GET['dbid'] );

		if( $row==false ){
			print "Diese ID existiert nicht<br>";
		} else {

			PrintUser( $row );
			print '<br><br>';
			PrintChangeTable($db,$db->GetChanges( $_GET['dbid'] ));

		};

	} else {
		print 'Es wurde keine ID angegeben';
	};


} else {

	print 'Es wurde keine ID angegeben';

}

include ('footer.php');

