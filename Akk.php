<?php
/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/

session_start();
//check if user is logged in
if(!isset($_SESSION['uid_akk']))
{
	Header("Location: index.php");
	exit(); 
}

require_once( 'AkkFunctions.php' );

/*
prints a hidden input field of name "active" and value "on" if something has happend in the last search

this includes akk oder deakk operation.

Furthermore it prints the field if your start over the application
*/
function PrintActive() {

	if( isset( $_POST['deakk'] ) or isset( $_POST['akk'] ) ){
		print '<input type="hidden" name="active" value="on">';
	};


	if( !isset( $_GET['superfield']) and !isset( $_GET['lastname']) and 
	 !isset( $_GET['exactlastname']) and !isset( $_GET['firstname'])  and 
	 !isset( $_GET['exactfirstname']) and !isset( $_GET['membershipid']) and 
	 !isset( $_GET['exactmembershipid']) ){
		print '<input type="hidden" name="active" value="on">';
	};
}

include ('header.php');

$db=new DataBase;

?>

<form method="get" action="Akk.php">
	<input type="hidden" name="reset" value="reset">
	<?php PrintActive(); ?>
	<input type="submit" value="Reset">
</form>

<form method="get" action="Akk.php" name="input" id="form1">
	<table>
		<tr>
			<td class='label'>McGyver-Suche</td>
			<td><input type="text" name="superfield" id="superfield" tabindex=1 value="<?php print GetGet('superfield'); ?>" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class='label'>Nachname</td>
			<td><input type="text" name="lastname" value="<?php print GetGet('lastname'); ?>" /></td>
			<td>Exakt: <input type="checkbox" name="exactlastname" <?php print GetGetChecked('exactlastname'); ?> ></td>
		</tr>
		<tr>
			<td class='label'>Vorname</td>
			<td><input type="text" name="firstname" value="<?php print GetGet('firstname'); ?>" /></td>
			<td>Exakt: <input type="checkbox" name="exactfirstname" <?php print GetGetChecked('exactfirstname'); ?> ></td>
		</tr>
		<tr>
			<td class='label'>Mitgliedsnummer</td>
			<td><input type="text" name="membershipid" value="<?php print GetGet('membershipid'); ?>" /></td>
			<td>Exakt: <input type="checkbox" name="exactmembershipid" <?php print GetGetChecked('exactmembershipid'); ?> ></td>
		</tr>
	</table>
	<?php
		PrintActive();
	?>
	<input type="submit" value="Suchen">

</form>
<?php PrintWarningAkk(); ?>
<hr />
<?php

ProcessAkkDeakk( $db );

$res=GetResult($db);

if( $res!=false ){
	PrintAkkTable( $db , $res );
};

include ('footer.php');

