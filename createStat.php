<?php

/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
* purpose:
* 
* This file generate some overall stats for official database
* e.X.: number of members, members which has paid, members which are online, all group by divison
*
*/


?>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<?php

require_once( 'database.php' );
$db=new DataBase;

?>

<table border="1">
<tr><td>LV</td><td>Anzahl Mitglieder</td><td>Anzahl Stimmberechtigte</td><td>Anzahl akkreditierte Mitglieder</td><td>Beteiligungsquote im LV</td><td>Beteiligungsquote im LVs(stimmberechtigte Mitglieder)</td><td>Beteiligung Total</td></tr>
<?php

$res=$db->GetDivisions();

/*
a little statistics for you
*/

$total = $db->GetStats( '*' , 3  );
$totalPaid = $db->GetStats( '*' , 4  );



$num=array(0,0,0);
$sum=array(0,0,0);

while( $row=$db->mysql_fetch_row($res ) ){

	print '<tr>';

	print '<td>'.$row[0].'</td>';

	$num[0]=$db->GetTotalByDivision( $row[0] , $paidlive=NULL , $akkreditiert=NULL  );
	$num[1]=$db->GetStats( $row[0] , 2  );
	$num[2]=$db->GetStats( $row[0] , 1  );

	$sum[0]+=$num[0];
	$sum[1]+=$num[1];
	$sum[2]+=$num[2];

	print '<td>'.$num[0].'</td>';
	print '<td>'.$num[1].'</td>';
	print '<td>'.$num[2].'</td>';
	print '<td>'.number_format($num[1]*100/$num[0],1).'</td>';
	print '<td>'.number_format($num[2]*100/$num[1],1).'</td>';
        print '<td>'.number_format($num[2]*100/$totalPaid,1).'</td>';

	print '</tr>';

};

print '<tr><td>Sum:</td><td>'.$sum[0].'</td><td>'.$sum[1].'</td><td>'.$sum[2].'</td><td>'.number_format($sum[1]*100/$sum[0],1).'</td><td>'.number_format($sum[2]*100/$sum[1],1).'</td></tr>';

?>
</table>

<?php

print '<br><br>Statistic made at '.$db->GetDBTime();

?>

</body>
</html>
