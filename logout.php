<?php

/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/


include ('header.php');

session_start();

unset($_SESSION['uid_akk']);
unset($_SESSION['id']);

print "<h3>Du bist raus.</h3>";
print "<h3>Neuer Login <a href='index.php'>hier</a>.</h3>";


include ('footer.php');

