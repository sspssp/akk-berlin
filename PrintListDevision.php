<?php

/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/


include ('header.php');

require_once( 'database.php' );

$db=new DataBase;

$res=$db->GetDivisions();

/*
shows all which has paid live, by deivsion
*/
while( $row=$db->mysql_fetch_row($res ) ){

	$res2=$db->GetLivePaid( $row[0] );


	print '<h1>'.$row[0].'</h1>';

	print '<table>';
	print '<thead><tr>
		<td>Mitgliedsnummer</td>
		<td>Nachname</td>
		<td>Vorname</td>
		<td>Zwischenname</td>
		<td>Geburtsdatum</td>
		<td>Kommentar</td>
	       </tr></thead>';

	while( $row2=$db->mysql_fetch_row( $res2 ) ){

		print '<tr>';
		print '<td>'.$row2[4].'</td>';
		print '<td>'.$row2[1].'</td>';
		print '<td>'.$row2[3].'</td>';
		print '<td>'.$row2[2].'</td>';
		print '<td>'.$row2[5].'</td>';
		print '<td>'.$row2[6].'</td>';
		print '</tr>';

	};

	print '</table>';

	print '<br>';

};

include ('footer.php');

