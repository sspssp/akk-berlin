/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/




drop database members;
CREATE database members;

use members;


/*
	members table
*/
CREATE TABLE members (
	dbid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	lastname CHAR(100) NOT NULL,
	middlename CHAR(100),
	firstname CHAR(100) NOT NULL,
	membershipid INT NOT NULL UNIQUE,
	birthdate DATE NOT NULL,
	adress CHAR(100),
	city CHAR(100),
	zipcode CHAR(5),
	division CHAR(100) NOT NULL,
	paid TINYINT(1) DEFAULT 1,
	akkreditiert TINYINT(1) DEFAULT 0,
	paidchanged TINYINT DEFAULT 0,
	acomment CHAR(100) DEFAULT "",
	warning INT NOT NULL DEFAULT 0,
	topay INT
) 
 CHARACTER SET utf8 COLLATE utf8_general_ci
;


LOAD DATA LOCAL INFILE '/home/wilm/Akk/all.csv' INTO TABLE members  
	CHARACTER SET utf8
	FIELDS TERMINATED BY ';'
	LINES TERMINATED BY '\n'  
	(lastname,middlename,firstname,membershipid,birthdate,adress,city,zipcode,division,paid,warning,topay);

update members set akkreditiert = 0;
update members set acomment = null;


/*
	and the user table
*/
CREATE TABLE users (
	userid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name CHAR(100) NOT NULL UNIQUE,
	hash CHAR(100) NOT NULL
);

INSERT INTO users (name,hash) VALUES ('akk1' , md5('akk1') );





CREATE TABLE changes (
	changeid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	userid INT NOT NULL,
	dbid INT NOT NULL,
	thechange INT NOT NULL,
	acomment CHAR(100),
	thetime DATETIME NOT NULL
);


