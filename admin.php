<?php
session_start();
/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/


?>


<html>
<head>

<style type="text/css">
	form { margin: 0; }
	span { display:inline; }


</style>


<script type="text/JavaScript" src="functions.js">

</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<?php

session_start();


if( isset( $_POST['adminlogout'] ) ){
	unset($_SESSION['admin']);
};

require_once( "database.php" );
$db=new DataBase();

// check input
if( isset( $_POST['adminpasswd'] ) ) {

	include( 'Constants.php' );

	if( $_POST['adminpasswd']==$adminpasswd ){

		$_SESSION['admin']=true;

	};


};


// check wheather i'm admin
if( !isset($_SESSION['admin']) ){
	
	print '<form method="post" action="admin.php">
	<table>
		<tr><td>Adminpasswd</td><td><input type="password" name="adminpasswd"></td></tr>
	</table>
	<form>';

	exit(0);

};


?>


<body onload="setFocus();doScroll();" onunload="window.name=document.body.scrollTop">
Admin Area
<div align="right"><form method="post" action="admin.php"><input type="hidden" value="true" name="adminlogout"><input type="submit" value="logout"></form></div>
<hr>


<?php


// build site

// process add user action

if( isset( $_POST['newusername'] ) and isset( $_POST['newuserpasswd1'] ) and isset( $_POST['newuserpasswd2'] ) ){

	// user exist?
	$num=$db->GetIDByName( $_POST['newusername'] );

	if( $_POST['newusername']=="" ){
		print( 'Username must not be ""<br>' );
	} else if( $_POST['newuserpasswd1']!=$_POST['newuserpasswd2'] ){
		print( 'passwds not the same<br>' );
	} else if( $_POST['newuserpasswd1']=="" ){
		print( 'passwd must not be ""<br>' );
	} else if( $num!=0 ){
		print 'user exists<br>';
	} else {

		// add user ...
		$db->AddUser( $_POST['newusername'] , $_POST['newuserpasswd1'] );
		$_POST['newusername']="";
	};



} else {
	$_POST['newusername']="";
};

print 'Add new User';
print 	'<form method="post" action="admin.php"><table>
		<tr><td>Name</td><td><input type="text" name="newusername" value="'.$_POST['newusername'].'"></td></tr>
		<tr><td>Passwd</td><td><input type="password" name="newuserpasswd1"></td></tr>
		<tr><td>Passwd (retype)</td><td><input type="password" name="newuserpasswd2"></td></tr>
	</table>
	<input type="submit" value="Add User">
	</form><br><br>';


// delete user

if( isset( $_POST['deleteid'] ) ){

	$db->DeleteUser($_POST['deleteid']);

};

if( isset( $_POST['changepasswd1'] ) and isset( $_POST['changepasswd2'] ) and isset( $_POST['changeid'] ) ){

	if( $_POST['changepasswd1']!=$_POST['changepasswd2'] ){
		print "passwds don't fit<br>";
	} else {
		$db->ChangePasswd( $_POST['changeid'] , $_POST['changepasswd2'] );
		print 'passwd changed<br>';
	};

};




$res=$db->GetAllUsers();


print 'Current Users';
print '<table border="1">';
print ' <tr><td>Id</td><td>Name</td></tr> ';

if( !isset( $_GET['id'] ) ){
	$_GET['id']="-1";
};

while( $row=$db->mysql_fetch_row( $res ) ){
	print '<tr><td><a href="admin.php?id='.$row[0].'">'.$row[0].'</a></td><td>'.$row[1].'</td>';

	if( $row[0]==$_GET['id'] ){
		print '<td>';

		print 'Change Passwd: <form method="post" action="admin.php?id='.$row[0].'">
		<table>
			<tr><td>New Passwd</td><td><input type="password" name="changepasswd1"></td></tr>
			<tr><td>New Passwd (retype)</td><td><input type="password" name="changepasswd2"></td></tr>
		</table>
		<input type="hidden" name="changeid" value="'.$row[0].'">
		<input type="submit" value="Change Passwd">
		</form>';

		print 'Delete User?
		<form method="post" action="admin.php"><input type="hidden" name="deleteid" value="'.$row[0].'"><input type="submit" value="Delete"></form>
		';
	

		print '</td>';
	};

	print '</tr>';

};

print "</table>";


?>


</body>
