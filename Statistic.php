<?php

/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/


include ('header.php');

require_once( 'database.php' );
$db=new DataBase;

?>

<table>
<thead>
	<tr><td>Name</td><td>Akkreditiert</td><td>Total</td><td>gezahlt</td></tr>
</thead>
<?php

$res=$db->GetDivisions();

/*
a little statistics for you
*/

$num=array(0,0,0);
$sum=array(0,0,0);

while( $row=$db->mysql_fetch_row($res ) ){

	print '<tr>';

	print '<td>'.$row[0].'</td>';

	$num[0]=$db->GetTotalByDivision( $row[0] , $paidlive=NULL , $akkreditiert=1  );
	$num[1]=$db->GetTotalByDivision( $row[0] , $paidlive=NULL , $akkreditiert=NULL  );
	$num[2]=$db->GetTotalByDivision( $row[0] , $paidlive=1 , $akkreditiert=NULL  );

	$sum[0]+=$num[0];
	$sum[1]+=$num[1];
	$sum[2]+=$num[2];

	print '<td>'.$num[0].'</td>';
	print '<td>'.$num[1].'</td>';
	print '<td>'.$num[2].'</td>';

	print '</tr>';

};

print '<tr><td class="thead">Summe:</td><td class="thead">'.$sum[0].'</td><td class="thead">'.$sum[1].'</td><td class="thead">'.$sum[2].'</td></tr>';

?>
</table>

<?php

print '<br><br>Statistic made at '.$db->GetDBTime();

?>

</body>
</html>
