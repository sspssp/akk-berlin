<?php
session_start();
/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/

include ('header.php');

function PrintFunctions()
{
	echo " <h2>Funktionen</h2>";
	echo " <ul id='menu'>";
	echo " <li class='menu'><a href='Akk.php'>Akkreditierung</a></li>";
	echo " <li class='menu'><a href='AkkSchatz.php'>Akkreditierung und Bezahlen</a></li> ";
	echo " <li class='menu'><a href='PrintListDevision.php'>Output für Schatzis</a></li> ";
	echo " <li class='menu'><a href='Statistic.php'>Statistik</a></li> ";
	echo " <li class='menu'><a href='Changes.php'>Änderungen</a></li> ";
	echo " <li class='menu'><a href='FullList.php'>Liste zum Ausdrucken</a></li> ";
	echo " </ul>";
};



function PrintInputForm()
{
	print "
	<form method='post' action='index.php' id='form1'>
	 <fieldset>
	 <legend>Login</legend>
	 <p><label for='loginname'>Benutzer</label><input type='text' name='loginname'></p>
	 <p><label for='loginpasswd'>Passwort</label><input type='password' name='loginpasswd'></p>
	 <p class='button'><input type='submit' value='Senden'></p>
	 </fieldset>
	</form>";
};


//check if user is logged in
if(isset($_POST['loginname']) and isset($_POST['loginpasswd']))
{
		//loginform used
	require_once('database.php');
	$db=new DataBase;

	$num=$db->CheckUserAndPw($_POST['loginname'],$_POST['loginpasswd']);


	if( $num==0 )
	{
		print '<h3>Falscher Benutzername oder Passwort!</h3>';
		PrintInputForm();
		exit(0);
	}
	else if( $num==1 )
	{
		$_SESSION['uid_akk']=$_POST['loginname'] ;
		$_SESSION['id']=$db->GetIDByName ( $_POST['loginname'] );

		PrintFunctions();

	}
	else
	{
		die('<h3>Das ging was mächtig schief. Abbruch</h3>');
	}
}
else if(!isset($_SESSION['uid_akk']) and !isset($_SESSION['id']))
{
	PrintInputForm();	
}
else //user is logged in correctly
{
	PrintFunctions();
}

include ('footer.php');

