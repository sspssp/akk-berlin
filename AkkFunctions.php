<?php


/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/


/*session_start();
//check if user is logged in
if(!isset($_SESSION['uid_akk']))
{
	
	Header("Location: index.php");
	exit(); 
}*/


require_once( 'database.php' );



/*
checks weather the user wants do akk/deakk something, and the executes it
*/
function ProcessAkkDeakk( $db ) {

	if( isset( $_POST['deakk'] ) ){
		$db->DeAkk( $_POST['deakk'] );
	};

	if( isset( $_POST['akk'] ) ){
		$db->Akk( $_POST['akk'] );
	};

	return 0;

};

/*
checks weather the user wants do check in a pay something, and the executes it
*/
function ProcessPay( $db ) {

	if( isset( $_POST['pay'] ) and isset( $_POST['comment'] ) ){
		$db->Pay( $_POST['pay'] ,  $_POST['comment'] );
	};

	if( isset( $_POST['unpay'] ) ){
		$db->UnPay( $_POST['unpay'] );
	};

	return 0;

};


/*
checks weather a get is given, and returns the string
"" if not defines
*/
function GetGet( $name ) {

	if( isset( $_GET[$name] ) ){
		return htmlspecialchars($_GET[$name]);
	} else {
		return "";
	};


};

/*
checks weather a get is given and is "on", and returns the "checked"
"" if not defines

this function is there for checkboxes html
*/
function GetGetChecked( $name ) {

	if( isset( $_GET[ $name ] ) ){

		if( $_GET[ $name ]=="on" ){
			return "checked";
		};

	};

	return "";



}

/* Returns checksum of an EAN8, else false */
function checkSumEAN8($input) {

        $ret=false;

        if (strlen($input) == 8 or strlen($input) == 7) {

                $sum1 = $input[1] + $input[3] + $input[5];
                $sum2 = $input[0] + $input[2] + $input[4] + $input[6];
                $checksum_value = $sum1 + $sum2 * 3;
                $checksum_value = 10 - ($checksum_value % 10);
                if ($checksum_value == 10) $checksum_value = 0;
                $ret = $checksum_value;
        }


        return $ret;
}


/*
	superfield search.
	returns the seacrh result in dependency of the input.
	if its a EAN8 string, it searches the matching memberschip id search
	if its a number, the normal membership id search is done
	if its a string, a lastname search ist done
*/
function SuperfieldSearch( $db , $superfield ){

	$intSuperfield = (int) $superfield;
	// 0 is false
	if ($intSuperfield) {

		$checksum_value = checkSumEAN8($superfield);
		$checksum_value2 = $intSuperfield % 10;

		// EAN8?
		if( $checksum_value === $checksum_value2 ) {
			// EAN8!
			$intSuperfield = (int)($intSuperfield / 10);
		};
		
		return $db->MemberschipIDSearch( $intSuperfield );

	} else {
		return $db->LastnameSearch( $superfield );
	};


	return $res;

};

/*
the main searching function
this methods decides which search is to do and is executing it

*/
function GetResult($db) {


	if( isset( $_GET['superfield'] ) and isset( $_GET['lastname'] ) and isset( $_GET['firstname'] ) 
		and isset( $_GET['membershipid'] )){


		// superfield on?
		if( $_GET['superfield']!="" ){

			return SuperfieldSearch( $db , $_GET['superfield'] );

		};

		// emnpty search?
		/*if( $_GET['lastname']=="" and $_GET['firstname'] =="" and $_GET['membershipid']=="" ){
			return false;
		};*/

		$lastnamelike=true;
		$firstnamelike=true;
		$membershipidlike=true;

		//exact?
		if( isset( $_GET['exactlastname'] ) ){
			if( $_GET['exactlastname']=="on" ){ $lastnamelike=false; };
		};
		if( isset( $_GET['firstname'] ) ){
			if( $_GET['firstname']=="on" ){ $firstnamelike=false; };
		};
		if( isset( $_GET['membershipid'] ) ){
			if( $_GET['membershipid']=="on" ){ $membershipidlike=false; };
		};


		$res=$db->GetSearch( $_GET['lastname'] , $_GET['firstname'] , $_GET['membershipid'] ,
		$lastnamelike=$lastnamelike , $firstnamelike=$firstnamelike , $membershipidlike=$membershipidlike );

		return $res;

	};


	return false;

};


/*
	get the current URL with all parameter stuff
*/
function GetCurrentURL (){

	$action=htmlspecialchars($_SERVER['REQUEST_URI']);

	/*$action=$_SERVER['PHP_SELF'].'?';

	foreach( $_GET as $key=>$v ){

		$action=$action.htmlspecialchars($key).'='.htmlspecialchars($v).'&';

	};

	$action=rtrim( $action , '&?' );*/

	return $action;

};

/*
prints a html post button with a button name, a name of a hidden input 
and the value of the hidden input
*/
function PrintButton( $buttonname , $name , $value  ) {

	$action=GetCurrentURL();

	print '<form method="post" action="'.$action.'" >';

	print '<input type="hidden" name="'.$name.'" value="'.$value.'">';

	print '<input type="submit" class="akkbutton" value="'.$buttonname.'"/>';

	print '</form>';


};

/*
	prints a html formatted row of basic informations of a seach result with a deakk akk button (depending on akk-status and paid-status)
*/
function PrintRowBasic( $row, $db = false ) {

//dbid,lastname,middlename,firstname,membershipid,birthdate,adress,city,zipcode,
//division,paid,akkreditiert,paidchanged,acomment,warning

	print '<td><a href="Changes.php?dbid='.$row[0].'">'.$row[0].'</a></td>';

	for( $i=1;$i<10;$i=$i+1 ){
		print '<td>'.$row[$i].'</td>';

	};


	if( $row[10]==1 ){
		if( $row[11]==1 ){
			print '<td></td><td>';


			PrintButton( "Deakk" , "deakk" , $row[0]  ) ;
			print '</td>';
		} else {
			print '<td>';


			PrintButton( "Akk" , "akk" , $row[0] ) ;
			print '<td></td></td>';
		};
	} else {
		print '<td></td><td></td>';
	};
	$streetArray = explode(" ", $row[6]);
	$hausnummer = "";
	$streetName = "";
		//foreach($streetArray as $isMaybeAStreetNumber)
		for($g=count($streetArray)-1; $g>=0;$g--)
		{
			$isMaybeAStreetNumber = $streetArray[$g];
			if($hausnummer!="")
			{
				$streetName = $isMaybeAStreetNumber ." ". $streetName;
			}
			else
			{
				for($i=0;$i<strlen($isMaybeAStreetNumber);$i++)
        	                {
	                                if(is_numeric(substr($isMaybeAStreetNumber, $i, 1)))
                                	{
                        	                $hausnummer .= substr($isMaybeAStreetNumber, $i, 1);
                	                        $isAHausnummer=true;
        	                        }
	                        }
			}
		}
		
	echo '<td>';
	$wahlkreis = $db->getWahlkreis($streetName, $hausnummer);
	 echo "<a target='_blank' href='Wahlkreis.php?street=".urlencode($streetName)."&hausnummer=".$hausnummer."'>";
	if(count($wahlkreis)==1)
	{
		echo $wahlkreis[0]["Bundestagswahlkreis"];
	}
	else
	{
		echo "??";
	}
	echo "</a>";
	echo'</td>';

};
function strposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
                $res = strpos($haystack, $needle, $offset);
                if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
}
/*
prints a row with akk an de deakk buttons, colored depending on status
*/
function PrintAkkRow( $row , $db = false){

	if( $row[14]!=0 ){
		print "<tr class=\"akkrow \" bgcolor=\"#FF00FF\">";
	} else {

		if( $row[10]==1 ){
			if( $row[11]==1 ){
				print "<tr class=\"akkrow \" bgcolor=\"#80FF80\">";
			} else {
				print "<tr class=\"akkrow \" bgcolor=\"#FFFF80\">";
			};
		} else {
			print "<tr class=\"akkrow \" bgcolor=\"#FF8080\" >";
		};
	};

	PrintRowBasic( $row , $db);

	print '</tr>';

};

/*
prints the whole table of search result with akk and deakk buttons
*/
function PrintAkkTable( $db , $res ) {

	$num=mysql_num_rows( $res );

	print $num.' Resultate<br>';

	if( $num<1 ){
		return 1;
	};

	print '<table class="akktable">';

	print '<thead><tr><td>Id</td><td>Nachname</td><td>Zwischenname</td><td>Vorname</td><td>Mitgliedsnummer</td>
		<td>Geburtsdatum</td><td>Adresse</td><td>Ort</td><td>PLZ</td><td>LV</td><td colspan="2">Funktionen</td><td>WK</td></tr></thead>';

	while( $row=$db->mysql_fetch_row( $res ) ){

		PrintAkkRow( $row , $db);

	};

	print '</table>';

	return 0;

};

/*
prints the form for paying and unpaying a contakt. For paying there is a field for comments
*/
function PrintPaidForm($id, $paid , $comment ) {

	$action=GetCurrentURL();

	print '<form method="post" action="'.$action.'">';

	if( $paid==1 ){
		print '<td></td><td>';
		print '<input type="hidden" name="unpay" value="'.$id.'">';
		print '<input type="submit" value="UnPay">';
		print '</td><td>'.$comment.'</td>';
	} else {
		print '<td>';
		print '<input type="hidden" name="pay" value="'.$id.'">';
		print '<input type="submit" value="Pay">';
		print '</td><td></td>';
		print '<td><input class="paycomment" type="text" name="comment" value="'.$comment.'"></td>';
	};


	print '</form>';

}

/*
prints a row of a search result with akk/deakk and pay/unpay buttons
*/
function PrintAkkRowFull( $row, $db = false) {


//dbid,lastname,middlename,firstname,membershipid,birthdate,adress,city,zipcode,
//division,paid,akkreditiert,paidchanged,acomment,warning,topay


	if( $row[14]!=0 ){
		print "<tr class=\"akkrow \" bgcolor=\"#FF00FF\">";
	} else {

		if( $row[10]==1 ){
			if( $row[11]==1 ){
				print "<tr class=\"akkrow \" bgcolor=\"#80FF80\">";
			} else {
				print "<tr class=\"akkrow \" bgcolor=\"#FFFF80\">";
			};
		} else {
			print "<tr class=\"akkrow \" bgcolor=\"#FF8080\" >";
		};
	};


	PrintRowBasic( $row , $db);
	
	print '<td>';
	if( $row[10]!=1 ){ print $row[15]."€"; };
	print '</td>';

	PrintPaidForm( $row[0] , $row[10] , $row[13] );

	print '</tr>';


};

/*
prints a full table of a search with akk/deakk pay/unpay buttons
*/
function PrintAkkTableFull( $db , $res ) {

	$num=mysql_num_rows( $res );

	print $num.' results<br>';

	if( $num<1 ){
		return 1;
	};

	print '<table class="akktable">';

	print '<thead><tr><td>Id</td><td>Nachname</td><td>Zwischenname</td><td>Vorname</td><td>Mitgliedsnummer</td>
		<td>Geburtsdatum</td><td>Adresse</td><td>Ort</td><td>PLZ</td><td>LV</td><td>Akkreditierung</td><td></td><td>WK</td><td>Offen</td><td colspan="2">Zahlungen</td><td>Kommentar</td></tr></thead>';

	while( $row=$db->mysql_fetch_row( $res ) ){


		PrintAkkRowFull( $row , $db);

	};

	print '</table>';

	return 0;

};

/*
prints a blinking warning, if now changes were made in the last search,
or a reset is done without a change
*/
function PrintWarningAkk() {

	if( !isset( $_GET['superfield']) and !isset( $_GET['lastname']) and 
	 !isset( $_GET['exactlastname']) and !isset( $_GET['firstname'])  and 
	 !isset( $_GET['exactfirstname']) and !isset( $_GET['membershipid']) and 
	 !isset( $_GET['exactmembershipid']) and !isset( $_GET['reset'] ) ){

		return 0;

	};

	if( isset( $_GET['reset'] ) and !isset( $_GET['active'] ) ){

		print '<blink><font size="5" color="red"><b>WARNUNG:<br>
		Du hast einen Reset seit der letzten Suche durchgeführt.<br>
		Hast du vielleicht etwas vergessen zu überprüfen?
		</b></font></blink>';

		print '<br>';

		return 1;

	};

	if( !isset( $_POST['deakk'] ) and !isset( $_POST['akk'] ) and !isset($_GET['active']) ) {

		print '<blink><font size="5" color="red"><b>WARNUNG:<br>
		Es wurde nichts an der letzten Suche geändert.<br>
		Hast du vielleicht etwas vergessen zu überprüfen?
		</b></font></blink>';

		print '<br>';

		return 1;

	};

};

?>
