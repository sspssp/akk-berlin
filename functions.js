
/*
* ----------------------------------------------------------------------------
* Hendrik Stiefel <hendrik@machmaldieaugenauf.de>, Jörg Franke <piratenpartei@newjorg.de> 
* and Wilm Schumacher <wilm.schumacher@piratenpartei.de> wrote this file. As 
* long as you retain this notice you can do whatever you want with this stuff. 
* If we meet some day, and you think this stuff is worth it, you can buy us a 
* beer in return
* 
* Hendrik, Jörg and Wilm
* ----------------------------------------------------------------------------
*/




//scroll function
function doScroll(){
	if (window.name) window.scrollTo(0, window.name);
}

//set autofocus
function setFocus() {
	var input = document.getElementById("input");
	if (input) {
		input["superfield"].focus();
		input["superfield"].select();
	}
}


